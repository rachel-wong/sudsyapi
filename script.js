// grab DOM elements (oh noes globals)
const btnSearch = document.getElementById('btnSearch')
const txtSearch = document.getElementById('food')
const resultArea = document.getElementById('result')
const btnClearAll = document.getElementById('btnClear')

btnClearAll.onclick = function () {
  txtSearch.value = ""
  resultArea.innerHTML = ""
}

btnSearch.onclick = function () {
  let searchTerm = txtSearch.value

  // data cleansing
  let regex = /\d+$/ // test for any numbers (true = "9", "ice9"; false = "icecream")
  if (regex.test(searchTerm)) {
    resultArea.innerHTML = "<div class='container zeroSearch'><h2>Can't serach for " + searchTerm + "!</h2><br/><br/><p>Only alphabetical names of food, no symbols or numbers.</p></div>"
  } else {
    const api = `https://api.punkapi.com/v2/beers?food=${searchTerm}` // from docs
    resultArea.innerHTML = "" // reset the result area afresh everytime the button is clicked
    // async because we don't know how long api will respond
    fetch(api)
      .then(response => {
        // returns a promise
        return response.json() // turn it into json object
      }).then(data => {
        // console.log("data: ", data)
        // parse the json object into javascript array of object 
        if (data.length === 0) {
          // 'icecream' returns no results so this is a catch all
          resultArea.innerHTML = "<div class='container zeroSearch'><h2>No tasty brewski for " + searchTerm + "!</h2><br/><br/><p>You have found the mythical food where no self-respecting beer could ever rise up to.</p><p>It's not the drink, it's you. Eat something else.</p></div>"
        } else {
          for (let i = 0; i < data.length; i++) {
            //destructuring too clumsy when you're passing the variables on
            let beerObject = {
              id: data[i].id,
              name: data[i].name,
              description: data[i].description,
              image_url: data[i].image_url,
              first_brewed: data[i].first_brewed,
              malt: data[i].ingredients.malt,
              hops: data[i].ingredients.hops,
              yeast: data[i].ingredients.yeast,
              food_pairing: data[i].food_pairing,
              brewers_tips: data[i].brewers_tips,
              contributed_by: data[i].contributed_by
            } // draw data out from the array of search results (api call)
            displayBeer(beerObject)
            txtSearch.value = ""
          }
        }
      })
      .catch(err => {
        const errorMessage = document.createElement('marquee')
        errorMessage.textContent = "Search API not working!"
        resultArea.appendChild(errorMessage)
        console.log(err.message)
      })
  }
}

// separate the view from the controller 
function displayBeer(beerInfo) {
  var {
    id,
    name,
    description,
    image_url,
    first_brewed,
    malt, // array of objects
    yeast,
    hops, // array of objects
    food_pairing,
    brewers_tips,
  } = beerInfo // destructure the silly argument again so we don't have to use longform references lest we stab ourselves in the eye

  // draw names out
  malt = malt.map(item => item.name).join(", ").trim()
  hops = hops.map(item => item.name)
  hops = [...new Set(hops)].join(", ").trim() // there are duplicates in hops for some reason

  // There has to be a better way of mapping things to DOM. 
  const card = document.createElement('div')
  card.setAttribute('class', 'card')

  const beerImg = document.createElement('img')
  beerImg.setAttribute('class', 'beerImg')
  if (image_url === null) {
    beerImg.src = "./assets/noImg.png"
    beerImg.classList.add('noImg')
  } else {
    beerImg.src = image_url
  }

  const mainTitle = document.createElement('h3')
  mainTitle.textContent = name

  const desc = document.createElement('p')
  desc.setAttribute('class', 'blurb')
  desc.textContent = description

  const pairing = document.createElement('p')
  pairing.setAttribute('class', 'blurb')
  pairing.innerHTML = "<h6>Pairs well with: </h6>" + food_pairing

  const brewTips = document.createElement('p')
  brewTips.setAttribute('class', 'blurb')
  brewTips.innerHTML = "<h6>Brewer's Tips: </h6>" + brewers_tips

  const beerContent = document.createElement('p')
  beerContent.setAttribute('class', 'blurb')
  beerContent.innerHTML = "<h6>Hops: </h6>" + hops + "<br/><br/>" + "<h6>Malt: </h6>" + malt + "<br/><br/>" + "<h6>Yeast: </h6>" + yeast

  const firstBrew = document.createElement('p')
  firstBrew.setAttribute('class', 'blurb')
  firstBrew.innerHTML = "First brewed on " + first_brewed

  card.appendChild(mainTitle)
  card.appendChild(beerImg)
  card.appendChild(desc)
  card.appendChild(pairing)
  card.appendChild(brewTips)
  card.appendChild(beerContent)
  card.appendChild(firstBrew)

  // card.innerHTML += h3.outerHTML + beerImg.outerHTML + p1.outerHTML + p2.outerHTML 
  resultArea.appendChild(card)
}
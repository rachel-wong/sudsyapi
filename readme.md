# Beer Pairing API in Vanilla JS

![Beer & Food Matching Walkthru](/assets/walkthru.gif)

### The Api

* [Punk API](https://punkapi.com/documentation/v2) also free, but 3600 requests/hour. No authentication.

### :eyes: Some general observations

The general structure of the fetch API

```javascript
// when button clicked, the program is triggered to move
btnSearch.addEventListener('click', () => {
  let searchTerm = txtSearch.value // txtSearch being the input form

  const api = `https://api.punkapi.com/v2/beers?food=${searchTerm}` // sending whatever the user inputted to the API

  // async because we don't know how long api will respond
  fetch(api).then(response => {
      // returns a promise
      return response.json() // turn it into json object
    }).then(data => {
      // parse the json object 
      console.log(typeof data, data)
    })
    .catch(err => {
      console.log(err.message)
    })
})
```

* The way I mapped objects onto the DOM is not efficient and quite ugly - essentially a separate constant is created for every object property. Could revert to using an array approach but why unravel a well-labelled object just to re-organise it again, seems like such a waste of effort/time.

* Error handling added for no results returned (i.e icecream :icecream: and beer :beer: don't go together apparently) and no image available.

| No Results  | No Image URL |
| ------------- | ------------- |
| ![No results](/assets/noResults.png)  | ![No image](/assets/nullImage.png)  |


### Other Resources

[Tania Rascia's API tutorial](https://www.taniarascia.com/how-to-connect-to-an-api-with-javascript/) (Recommended tutorial using the old `XMLHttpRequest` object)

[StackOverflow: from an array of objects, extract value of a property as an array](https://stackoverflow.com/questions/19590865/from-an-array-of-objects-extract-value-of-a-property-as-array). This helped me to return an array of malts and hops, which originally were an array of duplicating objects.

[Undraw again](https://undraw.co/illustrations)

[Reference](https://www.youtube.com/watch?v=Mc8-0UPpMVk)